package utils;

/**
 * contains the settings for the server
 * @author Chamodya Wimansha
 *
 */
public class ServerConfig {
//	192.168.8.102
	private static String ip = "192.168.8.102";
	
	public static String getIp() {
		return ip;
	}
}
